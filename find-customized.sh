#!/bin/bash

# author:    @dgoosens
# licence:   GPLv3+: GNU GPL version 3
# version:   1.0
# 
# Copyright  2017 stoempDEV.com.  License GPLv3+: GNU GPL version 3 or later http://gnu.org/licenses/gpl.html.
# This  is free software: you are free to change and redistribute it. There is NO WARRANTY, 
# to the extent permitted by law.

if [[ ${#} = 1 ]] && [[ ${1} =~ "help" ]]; then
	echo "======================================================================="
	echo "CUSTOMIZED FIND COMMAND"
	echo " ROOT PATH SHOULD BE SPECIFIED WITH THE \"-R\" ARGUMENT"
	echo " OTHER ARGUMENTS AND/OR EXPRESSIONS CAN BE PASSED ALONG IN RANDOM ORDER"
	echo " ASIDE FROM THAT, JUST CHECK OUT THE REGULAR FIND MANUAL"
	echo "======================================================================="
	echo 
	echo 
	find --help
	exit
fi

args="$*"

# regexOptions="( -(H|L|P))"
regexOptions="( ?-(H|L|P|D ?[^ ]*))"
regexPath="-R ?([^ ]*?)"

options=""
root=""
expressions=""

# find root location
if [[ $args =~ $regexPath ]]; then
	i=1
  n=${#BASH_REMATCH[*]}	

  root=${BASH_REMATCH[1]}
  args=${args/${BASH_REMATCH[0]}/""}
fi


# find options
len=${#args}
len2=0

while [[ $len > $len2 ]]; do
	len=${#args}
	if [[ $args =~ $regexOptions ]]; then
		options="$options${BASH_REMATCH[0]}"
    args=${args/${BASH_REMATCH[0]}/""}
	fi
	len2=${#args}
done

expressions="$args"

echo "find with:"
echo "  root location:  $root"
echo "  options:       $options"
echo "  expressions:   $expressions"

echo " > find $options $root $expressions"
echo

# execute
find $options $root $expressions