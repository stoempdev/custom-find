# custom-find

Root path should be specified with the "-R" argument  
Other arguments and/or expressions can be passed along in random order  
Aside from that, just check out the regular find manual

## Examples

```bash
# regular find
find  -P ./ -name example.txt 

# custom-find
./find-customized.sh -name example.txt -R ./ -P
## OR
./find-customized.sh -R ./ -name example.txt -P
## OR
./find-customized.sh -P -R ./ -name example.txt 

```


## Install

Don't forget to make the find-custom.sh file executable:

```bash
chmod + x find-custom.sh
```

If you want to execute the script from anywhere, you can add it to your binaries:

```bash
sudo mv find-custom.sh /usr/bin/find-custom
sudo chmod + x /usr/bin/find-custom
```